(use-modules (ice-9 match)
             (srfi srfi-1)
             (guix build-system gnu)
             (guix download)
             (guix gexp)
             (guix git-download)
             ((guix licenses) #:prefix license:)
             (guix packages)
             (guix utils)
             (gnu packages)
             (gnu packages guile))

(define %source-dir (dirname (current-filename)))

(package
  (name "shadows-of-darknet")
  (version "0.1.0-git")
  (source (local-file %source-dir
                      #:recursive? #t
                      #:select? (git-predicate %source-dir)))
  (build-system gnu-build-system)
  (arguments
   '(#:make-flags '("GUILE_AUTO_COMPILE=0")))
  (inputs (list guile-3.0-latest))
  (propagated-inputs (list guile-json-4))
  (synopsis "A cyberpunk espionage MUD for Matrix.")
  (description "A cyberpunk espionage MUD for Matrix in Guile Scheme, written for the Spring Lisp Game Jam 2023.")
  (home-page "https://codeberg.org/FluxHarmonic/shadows-of-darknet/")
  (license license:agpl3+))
