(use-modules (shadows env)
             (shadows auth)
             (shadows server)
             (matrix client))

;; Login and start the server
(let* ((env (load-env-file))
       (token (or (load-auth-token)
                  (login-matrix-user (assoc-ref env 'user-name)
                                     (assoc-ref env 'password))))
       (lobby-id (assoc-ref env 'lobby-id)))
  (parameterize ((matrix-access-token token))
    (format #t "~%Starting game server, listening in lobby ~a~%" lobby-id)
    (run-server lobby-id)))
