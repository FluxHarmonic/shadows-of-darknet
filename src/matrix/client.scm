(define-module (matrix client)
  #:use-module (json)
  #:use-module (web client)
  #:use-module (web response)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-11)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 optargs))

(define-public matrix-homeserver-url
  (make-parameter "https://matrix.org"))

(define-public matrix-access-token
  (make-parameter #f))

(define (matrix-request http-action path body access-token)
  (let-values (((response body)
                (http-action
                 (format #f "~a/_matrix/client/v3/~a" (matrix-homeserver-url) path)
                 #:body (if body (scm->json-string body) #f)
                 #:headers (if access-token
                               `((Authorization . ,(string-append "Bearer " access-token)))
                               '()))))
    (if (eqv? (response-code response) 200)
        (json-string->scm (utf8->string body))
        (error "HTTP request failed!" (utf8->string body)))))

(define-public (matrix-get path)
  (matrix-request http-get path #f (matrix-access-token)))

(define*-public (matrix-post path body #:key (use-access-token #t))
  (matrix-request http-post path body (and use-access-token
                                           (matrix-access-token))))
