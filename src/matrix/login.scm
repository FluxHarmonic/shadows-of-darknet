(define-module (matrix login)
  #:use-module (matrix client))

(define-public (login-user user-name password)
  (let ((response (matrix-post "login"
                               `(("type" . "m.login.password")
                                 ("identifier" . (("type" . "m.id.user")
                                                  ("user" . ,user-name)))
                                 ("initial_device_display_name" . "Shadows of Darknet Server")
                                 ("password" . ,password))
                               #:use-auth #f)))
    (if response
        (assoc-ref body "access_token")
        #f)))
