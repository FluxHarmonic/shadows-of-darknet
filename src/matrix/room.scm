(define-module (matrix room)
  #:use-module (matrix client)
  #:use-module (ice-9 optargs))

(define*-public (create-room name
                             #:key
                             (preset "public_chat")
                             (invited-users '())
                             (topic "")
                             (visibility "private")
                             local-alias)
  (let* ((body `(("name" . ,name)
                 ("preset" . ,preset)
                 ("invite" . ,invited-users)
                 ("room_alias_name" . ,local-alias)
                 ("visibility" . ,visibility)
                 ("topic" . ,topic)))
         (response (matrix-post "createRoom"
                                (if local-alias
                                    (acons "room_alias_name"
                                           local-alias
                                           body)
                                    body))))
    (assoc-ref "room_id" response)))

(define*-public (leave-room room-id #:key reason)
  (matrix-post (format #f "rooms/~a/leave" room-id)
               (if reason
                   `(("reason" . ,reason))
                   '())))

(define-public (forget-room room-id)
  (matrix-post (format #f "rooms/~a/forget" room-id)
               #f))

(define-public (kick-room-member room-id user-id)
  (matrix-post (format #f "rooms/~a/kick" room-id)
               `(("reason" . "Closing the game room.")
                 ("user_id" . ,user-id))))
