(define-module (shadows auth)
  #:use-module (matrix login)
  #:use-module (ice-9 textual-ports))

;; Load the .auth-token file and return the token if it exists
(define-public (load-auth-token)
  (let ((token-file ".auth-token"))
    (if (file-exists? token-file)
        (get-string-all (open-input-file token-file))
        #f)))

(define (save-auth-token token)
  (let ((token-file ".auth-token"))
    (call-with-output-file token-file
      (lambda (port)
        (display token port)))))

(define-public (login-matrix-user user-name password)
  (format #t "Logging in as ~a~%" user-name)
  (let ((token (login-user user-name password)))
    (if token
        (begin
          (format #t "Logged in successfully!~%")
          token)
        (error "Could not authenticate successfully!"))))
