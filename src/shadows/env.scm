(define-module (shadows env))

(define-public (load-env-file)
  "Load environment settings from a file stored in .env file, stored as an alist"
  (let ((env-file ".env"))
    (if (file-exists? env-file)
        (read (open-input-file env-file))
        (error "No .env file found, cannot authenticate with Matrix"))))
