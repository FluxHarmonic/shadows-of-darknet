(define-module (shadows event)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-11)
  #:use-module (matrix client)
  #:use-module (ice-9 pretty-print)

  #:export (message?
            message-sender
            message-content
            sync-events))

(define-record-type message-event
  (make-message-event sender content)
  message?
  (sender message-event-sender)
  (content message-event-content))

(define (read-event-content event)
  (let ((event-type (assoc-ref event "type"))
        (event-content (assoc-ref event "content")))
    (cond
     ((eqv? event-type "m.room.message")
      (make-message-event (assoc-ref event-content "sender")
                          (assoc-ref event-content "body")))
     ((eqv? event-type "m.room.member")
      (let ((membership (assoc-ref event-content "membership"))
            (sender (assoc-ref event "sender")))
        (format #t "MEMBER ~a: ~a~%" sender membership)))
     (else #f))))

(define (read-room-events sync-response)
  ;(format #t "Sync response: ~a~%" sync-response)
  (let ((rooms (assoc-ref sync-response "rooms")))
    (if rooms
        (map (lambda (room)
               (let* ((room-id (car room))
                      (room-timeline (assoc-ref (cdr room) "timeline"))
                      (room-events (assoc-ref room-timeline "events")))
                 (cons room-id
                       (filter identity
                               (map read-event-content
                                    (vector->list room-events))))))
             ;; TODO: Handle other kinds of room types
             (or (assoc-ref rooms "join")
                 '()))
        '())))

(define (sync-events batch-token)
  (let* ((response (matrix-get (string-append "sync"
                                              (if batch-token
                                                  (string-append "?since=" batch-token)
                                                  ""))))
         (next-batch (assoc-ref response "next_batch")))
         `((next-batch . ,next-batch)
           (room-events . ,(read-room-events response)))))
