(define-module (shadows server)
  #:use-module (matrix room)
  #:use-module (shadows event))

(define (handle-lobby-events lobby-id events)
  #f)

(define (handle-player-events room-id events)
  #f)

(define-public (run-server lobby-id)

  ;; (create-room "Shadows of Darknet - Player Room"
  ;;              #:preset "private_chat"
  ;;              #:invited-users '("@daviwil:matrix.org")
  ;;              #:topic "A private room for playing Shadows of Darknet.")

  (let loop ((batch-token #f))
    (display ".")
    (let* ((events (sync-events batch-token))
           (batch-token (assoc-ref events 'next-batch))
           (events (assoc-ref events 'room-events)))

      ;; Process room events
      (for-each (lambda (room)
                  (let ((room-id (car room))
                        (events (cdr room)))
                    (format #t "\nRoom: ~a~%" room-id)
                    (if (equal? room-id lobby-id)
                        (handle-lobby-events room-id events)
                        (handle-player-events room-id events))))
                events)

      ;; Sleep a bit and then request the next batch
      (sleep 1)
      (loop batch-token))))
